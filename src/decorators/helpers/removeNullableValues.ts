const removeNullableValues = (inObject: any) => {
  let value;

  if (typeof inObject !== 'object' || inObject === null) {
    return inObject;
  }

  const outObject: any = Array.isArray(inObject) ? [] : {};

  // eslint-disable-next-line guard-for-in
  for (const key in inObject) {
    value = inObject[key];

    if (value !== '' && value !== null) {
      outObject[key] = removeNullableValues(value);
    }
  }

  return outObject;
};

export default removeNullableValues;
