const validator = (min = 1, max = 1000000) => ({
  pattern: new RegExp(`^.{${min},${max}}$`),
  message: `Value should be more than ${min} and less than ${max} character`,
});

export default validator;
