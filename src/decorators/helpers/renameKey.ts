export const renameKey = (object: Record<string, any>, key: string, newKey: string) => {
  const clonedObj = { ...object };
  clonedObj[newKey] = clonedObj[key];
  delete clonedObj[key];

  return clonedObj;
};
