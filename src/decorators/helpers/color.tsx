import ColorService from '@api/services/ColorService';

import { ColorData } from '@types';

import range from './range';

export const makeColorsData = async (linesCount: number): Promise<ColorData[]> => {
  const availableLines = range(linesCount);
  const colors = await ColorService.getAll();

  return colors.map(({ color }) => ({
    availableLines: [...availableLines],
    lines: 0,
    color,
  }));
};

export const processColorsData = (data: ColorData[], linesCount: number) => {
  const clonedData = JSON.parse(JSON.stringify(data)) as ColorData[];
  const filteredData = clonedData.filter(item => 2 ** (linesCount + 1) - 1 !== item.lines);

  for (const filteredDataItem of filteredData) {
    filteredDataItem.availableLines = range(linesCount + 1).filter(
      // eslint-disable-next-line no-bitwise
      num => !((2 ** num) & filteredDataItem.lines),
    );
  }

  return filteredData;
};
