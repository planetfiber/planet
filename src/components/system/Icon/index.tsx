import React, { FC } from 'react';
import { ReactComponent as more } from '../assets/icons/more.svg';
import { ReactComponent as plus } from '../assets/icons/plus.svg';
import { ReactComponent as close } from '../assets/icons/close.svg';
import { ReactComponent as search } from '../assets/icons/search.svg';
import { ReactComponent as search2 } from '../assets/icons/search2.svg';
import { ReactComponent as burger } from '../assets/icons/burger.svg';
import { ReactComponent as circle } from '../assets/icons/circle.svg';
import { ReactComponent as loading } from '../assets/icons/loading.svg';
import { ReactComponent as dropdown } from '../assets/icons/dropdown.svg';
import { ReactComponent as close2 } from '../assets/icons/close-bar.svg';
import { ReactComponent as deleteOpen } from '../assets/icons/delete_open.svg';
import { ReactComponent as loadingDark } from '../assets/icons/loading_dark.svg';
import { ReactComponent as dropdownDark } from '../assets/icons/dropdown_dark.svg';
import { ReactComponent as calendarArrow } from '../assets/icons/calendar_arrow.svg';


const icons = {
  more,
  plus,
  close,
  search,
  circle,
  loading,
  dropdown,
  deleteOpen,
  loadingDark,
  dropdownDark,
  calendarArrow,
  burger,
  close2,
  search2
};

export type IconName = keyof typeof icons;

interface IconProps {
  name: IconName;
  [x: string]: any;
}

const Icon: FC<IconProps> = ({ name, ...props }) => {
  const Component = icons[name];
  return <Component {...props} />;
};

export default Icon;
