import styled, { css, fontWeights } from '@styles';
import { TextColor } from '@styles/theme/themes';

export const sizes = {
  xs: css`
    color: ${p => p.theme.text.primary};
    font-size: 12px;
    font-weight: 400;
    line-height: 16px;
    letter-spacing: 0.01em;
  `,
  s: css`
    color: ${p => p.theme.text.primary};
    font-size: 14px;
    font-weight: 400;
    line-height: 19px;
    letter-spacing: 0.01em;
  `,
  m: css`
    color: ${p => p.theme.text.primary};
    font-size: 16px;
    font-weight: 400;
    line-height: 21px;
    letter-spacing: 0.01em;
  `,
  l: css`
    color: ${p => p.theme.text.primary};
    font-size: 20px;
    font-weight: 400;
    line-height: 27px;
    letter-spacing: 0.01em;
  `,
  xl: css`
    color: ${p => p.theme.text.primary};
    font-size: 24px;
    font-weight: 400;
    line-height: 32px;
    letter-spacing: 0.01em;
  `,
  '2xl': css`
    color: ${p => p.theme.text.primary};
    font-size: 28px;
    font-weight: 400;
    line-height: 37px;
    letter-spacing: 0.01em;
  `,
  '3xl': css`
    color: ${p => p.theme.text.primary};
    font-size: 36px;
    font-weight: 400;
    line-height: 48px;
    letter-spacing: 0.01em;
  `,
  '4xl': css`
    color: ${p => p.theme.text.primary};
    font-size: 46px;
    font-weight: 400;
    line-height: 61px;
    letter-spacing: 0.01em;
  `,
};

export type TextSizes = keyof typeof sizes;
export type FontWeights = keyof typeof fontWeights;

interface StyledProps {
  sz: TextSizes;
  clr?: TextColor;
  weight?: FontWeights;
}

export const Tag = styled.div<StyledProps>`
  /* size */
  ${({ sz }) => sizes[sz]}

  /* font weight */
  ${({ weight }) =>
    weight &&
    css`
      font-weight: ${fontWeights[weight]};
    `}

  /* color */
  ${({ clr }) =>
    clr &&
    css`
      color: ${p => p.theme.text[clr]};
    `}
`;
