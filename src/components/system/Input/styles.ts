import { Input as AntInput, InputNumber as AntInputNumber } from 'antd';
import styled from '@styles';

//  TODO: shake inputs when error
//  const shakeAnimation = keyframes`
//  0% { transform: translateX(3px); }
// 	20% { transform: translateX(-3px); }
// 	40% { transform: translateX(1.5px); }
// 	60% { transform: translateX(-1.5px); }
// 	80% { transform: translateX(0.8px); }
// 	100% { transform: translateX(0); }
// `;

export const Input = styled(AntInput)`
width: inherit;
height: 58px;
background: #FFFFFF;
border: 1px solid #D1D3D4;
border-radius: 3px;
padding-left: 5px;
font-size: 16px; 

  
:focus-visible{
  outline: 1px solid #00909E;
  border: 1px solid #00909E;
 } 



   &.search {
    max-width: 657px;
    height: 45px;
    border:none;
    border-radius: 50px 0px 0px 50px;
    font-size: 20px;
    padding-left: 24px;
    margin-right: 5px;
    
    /* :focus-visible{
      outline: 1px solid #00909E;
      
    } */
    ::placeholder {
      color: rgba(25, 41, 59, 0.7);
      font-size: 20px;
      font-style: italic;
    }
  }
  
`;

export const InputNumber = styled(AntInputNumber)`
  /* TODO: shake inputs when error */
`;
