import React, { FC } from 'react';
import { Row, Col } from 'antd';

import * as styles from './styles';
import Button from '@planetkit/Button';
import { Link, } from 'react-router-dom';
import { useStores } from '@store';
import { Content } from 'antd/lib/layout/layout';
import NoImage from '@assets/images/NoImage.png'
import { BASE_URL } from '@api/config';


const PlanetCard: FC<any> = ({ card }) => {

  const { layoutStore } = useStores();
  const getCardImagePath = (files: any) => {
    return files.length ? `${BASE_URL}/${files[0].path}/${files[0].name}${files[0].extension}` : NoImage
  }



  return <>
    <styles.Card

      cover={
        <Row justify='center' align='middle'>

          <Col span={20} className='cardRout'>
            <Link to={`/product/${card.id}`} onClick={layoutStore.hideService}>
              <img
                alt="example"
                src={getCardImagePath(card.files)}
              />
            </Link>

          </Col>
        </Row>
      }
    >
      <Content className='card-content'>

        <Row className='title' >
          <Col>
            <span>
              {card.title}
            </span>
          </Col>
        </Row>
        {card.price && <Row className='price'> {card.price} AMD</Row>}
        <Row className='count-content' justify='start' align='middle'>

          <Col sm={12} className='actions'>
            <Button type='text' icon='circle' className='small-icon' >
              -
            </Button>
            <span className='count'>1</span>
            <Button type='text' icon='circle' className='small-icon' >
              +
            </Button>

          </Col>
          <Col sm={12} >
            {card.availability ? <span className='status' aria-disabled>Available</span> : <span className='status status-disable' aria-disabled>Unavailable</span>}

          </Col>
        </Row>
        <Row justify='center' align='middle'>
          <Col span={24} className="add-card-content">
            <Button type='primary' className='add-card-btn' disabled >
              Add Card
            </Button>
          </Col>
        </Row>
      </Content>
    </styles.Card>


  </>
}

export default PlanetCard;