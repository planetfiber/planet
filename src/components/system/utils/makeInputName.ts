export const makeInputName = (name: string | number | (string | number)[]) => {
  if (name instanceof Array) {
    return name.join('_');
  }

  return name;
};
