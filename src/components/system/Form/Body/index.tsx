import React, { FC } from 'react';
import cn from 'classnames';
import { BRAND_CLASS_NAME } from '@styles/constants';
import * as styles from './styles';

interface Props {}

const Body: FC<Props> = ({ children }) => (
  <styles.Body className={cn(`${BRAND_CLASS_NAME}-form-body`)}>{children}</styles.Body>
);

export default Body;
