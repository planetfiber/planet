import React, { FC } from 'react';
import { Form as AntdForm, FormProps as AntdFormProps } from 'antd';
import Body from './Body';

import FormItem from './FormItem';
import { FormObserver } from './formObserver';
import { CustomFormContextProvider, useCustomFormObserver } from './context';

type FormProps = AntdFormProps & {
  formObserver?: FormObserver;
};

const Form: FC<FormProps> = ({ formObserver, scrollToFirstError, ...props }) => {

  if (formObserver) {
    return (
      <CustomFormContextProvider formObserver={formObserver}>
        <AntdForm scrollToFirstError={scrollToFirstError ?? true} {...props} />
      </CustomFormContextProvider>
    );
  }
  return <AntdForm {...props} />;
};

export default Object.assign(Form, {
  Body,

  Item: FormItem,
  List: AntdForm.List,
  useForm: AntdForm.useForm,
  Provider: AntdForm.Provider,
  ErrorList: AntdForm.ErrorList,
  useFormObserver: useCustomFormObserver, // --- custom
});
