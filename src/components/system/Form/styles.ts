import styled, { breakpoints, keyframes } from '@styles';
import { BRAND_CLASS_NAME } from '@styles/constants';

const shakeAnimation = keyframes`
  0% { transform: translateX(3px); }
	20% { transform: translateX(-3px); }
	40% { transform: translateX(1.5px); }
	60% { transform: translateX(-1.5px); }
	80% { transform: translateX(0.8px); }
	100% { transform: translateX(0); }
`;

interface WrapperProps { }

export const FormWrapper = styled.div.attrs(() => ({
  className: `${BRAND_CLASS_NAME}-form-wrapper`,
})) <WrapperProps>`
  --form-padding: 40px;

  display: grid;
  place-content: center;
  padding: 0 var(--form-padding);



  .col {
    display: flex;
    justify-content: space-between;
    margin-top: 20px;
    width: 100%;

    .${BRAND_CLASS_NAME}-dynamic-group-label {
      width: 100% !important;
    }

    .${BRAND_CLASS_NAME}-dynamic-group-outer,
    .ant-form-item {
      width: 48%;
    }
  }

  .counter-options-wrapper {
    width: 48%;
    display: flex;

    .counter-options {
      display: flex;
      align-items: center;
      justify-content: space-around;
      border: 1px solid;
      width: 70px;
      margin-left: 5px;
      border-radius: 5px;
      border-color: ${p => p.theme.input.border.default};
    }

    .ant-form-item {
      width: 100%;
    }

    .ant-form-item-control-input-content {
      display: flex;
    }
  }

  .upload-wrapper {
    margin-top: 25px;
    display: flex;

    .ant-upload-list-picture {
      min-width: 150px;
      max-width: 150px;
      margin-right: 10px;
    }
  }

  &.shake-invalid-form-inputs {
    .ant-form-item-has-error {
      will-change: padding;
      animation: ${shakeAnimation} 0.4s linear;
    }
  }

  form {
    width: 800px;

    ${breakpoints.down('xs')} {
      width: 100%;
    }

    .${BRAND_CLASS_NAME}-form-body {
      padding: 20px 0;

      > *:not(:last-child) {
        margin: 0 0 32px 0;
      }

      select {
        width: 100%;
        padding: 8px 4px;
        border: 1px solid lightgray;
      }
    }
  }
`;
