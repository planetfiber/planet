import { Form } from 'antd';
import styled from '@styles';
import { BRAND_CLASS_NAME } from '@styles/constants';

export const FormItem = styled(Form.Item)`
  display: block;
  position: relative;
  margin-bottom: unset;

  .ant-form-item-control-input-content {
  width:fit-content;
 }

 .ant-form-item-explain-error {
  color: red;
 }


  .ant-form-item-label {
    z-index: 2;
    /* height: 20px;
     */
    height: 100%;
    position: absolute;
    pointer-events: none;
    will-change: transform;
    transition: all 0.2s ease;
    transform-origin: center left;
    transform: translate(13px, 0) scale(1);
    


    label {
      color: inherit;
      height: inherit;
      font-size: 16px;
      line-height: 100%;
      letter-spacing: 0.06em;
      position: relative;
      top: calc(50% - 8px);

      ::before {
      }

      ::after {
        content: none !important;
      }
    }
  }

  .ant-form-item-control {
    .ant-form-item-control-input {
      .ant-form-item-control-input-content {
        .ant-input {
        }
      }
    }

    .ant-form-item-explain {
      top: 100%;
      position: absolute !important;

      &.ant-form-item-explain-error {
      }
      &.ant-form-item-explain-success {
      }
    }
  }

  &.${BRAND_CLASS_NAME}-form-item-field {
    .ant-form-item-label {
      padding: 0 6px;
      height: fit-content;
      transform: translate(7px, -50%) scale(0.75);

      label {
        line-height: 100%;
      }
    }
  }

  &.ant-form-item-has-error {
    .planet-input-styles{
        border: 1px solid red !important;
      }
    .ant-form-item-label {
      color: red;
    
    }

    .ant-form-item-control {
      .ant-form-item-control-input {
        .ant-form-item-control-input-content {
          .ant-input,
          .ant-select,
          .ant-input-number-input {
          
          }

          .ant-input,
          .ant-select {
          }
        }
      }
    }
  }

  /* &.${BRAND_CLASS_NAME}-form-item-focused {
    .ant-form-item-label {
      padding: 0 6px;
      height: fit-content;
      transform: translate(7px, -50%) scale(0.75);

      label {
        line-height: 100%;
      }
    } */
    &.${BRAND_CLASS_NAME}-form-item-focused {
    .ant-form-item-label {
      padding: 0 6px;
      height: fit-content;
      color: #00909E;
      transform: translate(7px, -50%) scale(0.75);
      background-color: white;

      label {
        line-height: 100%;
      }
    }

    .ant-form-item-control {
      .ant-form-item-control-input {
        .ant-form-item-control-input-content {
          .ant-input,
          .ant-select,
          .ant-input-number-input {
            outline: none;
            box-shadow: unset !important;
          }
        }
      }
    }
  }
`;
