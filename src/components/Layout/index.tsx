import React, { FC } from 'react';


import GlobalNavigation from './Navigation/GlobalNavigation';
import ServiceNavigation from './Navigation/ServiceNavigation';
import layoutImg from "@assets/images/layoutImg.png";

import { Wrapper, LayoutWrapper } from './styles';
import Footer from './Footer'
import Main from './Main';
import { Content } from 'antd/lib/layout/layout';
import { Row, Col } from 'antd';

interface LayoutProps { }

const Layout2: FC<LayoutProps> = ({ children }) => {



  return (
    <>
      <Wrapper>
        {/* <Image src={layoutImg} preview={false}  style={{position:"fixed"}}  /> */}

        <div className='abc'>
          <img src={layoutImg} alt='888' />
        </div>

        <LayoutWrapper hasSider>
          {/* <LayoutWrapper > */}


          {/* <Layout style={{ borderRadius: "30px" }}> */}
          <Content>

            <Row>

              <Col span={24}>
                <GlobalNavigation />
              </Col>
              <Col sm={24} md={8} lg={8} xl={6}>
                <ServiceNavigation />
              </Col>
              <Col sm={24} md={16} lg={16} xl={18}>
                <Main>{children}</Main>
              </Col>

            <Footer />
            </Row>
          </Content>
          {/* </Layout> */}
        </LayoutWrapper>

      </Wrapper>
    </>
  );
};
export default Layout2;

