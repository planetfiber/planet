import React, { FC, useRef } from 'react';
import { Link } from "react-router-dom";

import { Menu, Col, Row } from "antd";

import Button from '@planetkit/Button';


interface MenuProps {
  tabs:
  {
    name: string;
    tab: string;
  }[]
}


const MenuContainer: FC<MenuProps> = ({ tabs }) => {
  const ref = useRef<HTMLDivElement>(null);


  return (
    <div ref={ref} className='menu-content'>

      <Menu
        className=" menu-content"
        mode="horizontal"
        defaultSelectedKeys={["1"]}
        disabledOverflow={true}
      >




        <Row justify='end'>
          {tabs.map(({ name, tab }) => (
            <Menu.Item key={`${tab}-tab`}>
              <Col sm={24} md={24} lg={3} xl={2}>
                <Link to={`/${tab}`} >
                  < Button type='text' className='menu-item' >
                    {name}
                  </Button>
                </Link>
              </Col>
            </Menu.Item>
          ))}
        </Row>


      </Menu>
    </div>
  )
}

export default MenuContainer