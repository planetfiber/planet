import React, { FC, memo, useRef, useState } from 'react';

// import ThemeSwitch from '@planetkit/ThemeSwitch';

import { Link } from "react-router-dom";

import { Col, Row, Drawer } from "antd";
import logoSmall from "@assets/images/smallLogo.svg";



import Button from '@planetkit/Button';
import { Input } from '@planetkit/Input/styles';

import { useStores } from '@store';
import { useOutsideClickClose } from '@hooks/useOutsideClickClose';


import * as styles from './styles';
import './drawer.css'
import LoginContainer from './LogInContainer';
import MenuContainer from './MenuContainer';
import ResponsiveBar from './ResponsiveBar';

const tabs = [
  { name: 'General', tab: 'general' },
  { name: 'About Us', tab: 'about_us' },
  // { name: 'Delivery', tab: 'delivery' },
  { name: 'Partners', tab: 'partner' },
  // { name: 'Blog', tab: 'blog' },
  // { name: 'Contacts', tab: 'contacts' },

];


const GlobalNavigation: FC<any> = () => {
  // const { icons } = useTheme();

  const [visible, setVisible] = useState(false)


  const showDrawer = () => {
    setVisible(true)
  };
  const onClose = () => {
    setVisible(false)
  };


  const { layoutStore } = useStores();

  const ref = useRef<HTMLDivElement>(null);


  useOutsideClickClose(ref, () => layoutStore.isOpenService && layoutStore.hideService());


  return (
    <styles.Wrapper className='global-navigation site-layout-background navigation-content '>

      <div className="top-menu">
        <LoginContainer />
      </div>
      <div className="bottom-menu">
        <MenuContainer tabs={tabs} />
      </div>
      <Row justify='space-between' className='responsive-bar-actions' >
        <Col>
          <Button type='text' className='bars-menu' icon='burger' onClick={showDrawer} />
        </Col>
        <Col className='small-logo'>
          <Link to="/" >
            <img src={logoSmall} alt="logo" />
          </Link>
        </Col>
        < Button type='text' className='sign-up sign-up-small' size='small' disabled  >
          Sign Up
        </Button>

      </Row>
      <Row justify='space-between' className='responsive-bar-actions '  >
        <Col span={20} className='small-search-content '>
          <Input
            className='search search-small'
            type='search'
            autoComplete='off'
            placeholder='Search'
          />
        </Col>
        <Col span={4} className='small-search-content '>
          <Button type='primary' className='search-btn ' icon='search2' />
        </Col>

      </Row>



      <Drawer
        placement="top"
        onClose={onClose}
        closeIcon={<Button type='text' icon="close2" />}
        visible={visible}
        height={"calc(100% - 40px)"}
        className="drawer-nav-bar"
      >
        <ResponsiveBar tabs={tabs} close={onClose} />
      </Drawer>


    </styles.Wrapper >
  );
};

export default memo(GlobalNavigation);


