import React, { FC, useRef, useState } from 'react';
import { Link } from "react-router-dom";

import { observer } from 'mobx-react-lite';
import categoriesLogo from "@assets/images/categories.svg";



import { useStores } from '@store';
import { useOutsideClickClose } from '@hooks/useOutsideClickClose';

import { StyledServiceNavigation } from './styles';
import { Col, Menu, Row, Collapse, Dropdown } from 'antd';
import { Content } from 'antd/lib/layout/layout';




const Categories: FC<any> = ({ tabs }) => {
  const ref = useRef<HTMLDivElement>(null);

  const { layoutStore } = useStores();

  const [visible, setVisible] = useState(false)




  useOutsideClickClose(ref, () => layoutStore.isOpenService && layoutStore.hideService());


  const categoriesProductHandler = (e: any) => {
    setVisible(false)

  }


  const { Panel } = Collapse;


  const onChange = (key: string | string[]) => {
  };






  const renderTreeNodes = (data: any[]) =>
    data.map((item): any => {
      if (item.subCategories) {
        return (
          <Collapse expandIconPosition={'end'} ghost key={`${item.id}-sub`}>
            <Panel header={item.title} key={item.id} >
              {renderTreeNodes(item.subCategories)}
             
            </Panel>
          </Collapse>

        );
      }
      return (
        <Link to={`/product/categoryIds/${item.id}`} onClick={categoriesProductHandler} className="categories-link" id={item.id}>
          {item.title}

        </Link>
      )
    });


  const menu = (
    <Menu className='categories-menu'>

      {tabs.map((tab: any) => (
        <Col span={24} className='categories-menu-item'key={tab.id}>

          <Collapse expandIconPosition={'end'} destroyInactivePanel={true} ghost className='categories-parent' >
            <Panel header={tab?.title} key={tab.id} className='accordion-header-nested'>
              {renderTreeNodes(tab.subCategories)}
            
            </Panel>
          </Collapse>
        </Col>
      ))}


    </Menu>
  )






  return (
    <StyledServiceNavigation >


        <Content className='content' ref={ref}>
          <Row justify='start'>
            <Col span={24} className='header'>
              <Row>
                <Col md={7} lg={6} xl={5}>
                  <span className='categories-logo'>
                    <img src={categoriesLogo} alt='categories-logo' />
                  </span>

                </Col>
                <Col md={17} lg={18} xl={19}>
                  <span >Categories</span>
                </Col>

              </Row>
            </Col>

            < Dropdown overlay={menu} trigger={['click']} className="categories-dropdown" visible={visible} onVisibleChange={(val)=>setVisible(val)} >
              <Col span={24} className='small-header' onClick={()=>setVisible(prev=>!prev)}>
                <span className='categories-logo'>
                  <img src={categoriesLogo} alt='categories-logo'  />
                </span>
                <span style={{ alignItems: "center", display: "flex" }} >Categories</span>
              </Col>
            </Dropdown >

            <Menu className='categories-menu-large categories-menu'>

              {tabs.map((tab: any) => (
                <Col span={24} className='categories-menu-item' key={`${tab.id}-category-tab`}>
                  <Collapse onChange={onChange} expandIconPosition={'end'} destroyInactivePanel={true} ghost className='categories-parent'>
                    <Panel header={tab?.title} key={tab.id} className='accordion-header-nested' >
                      {renderTreeNodes(tab.subCategories)}
                    </Panel>
                  </Collapse>
                </Col>
              ))}


            </Menu>

          </Row>
        </Content>
    </StyledServiceNavigation >
  );
};

export default observer(Categories);





