import React, { FC, useRef } from 'react';
import { BrowserRouter as Router, Link } from "react-router-dom";

import { observer } from 'mobx-react-lite';

import Main from '../../Main';


import { useStores } from '@store';
import { useOutsideClickClose } from '@hooks/useOutsideClickClose';

import { StyledServiceNavigation } from './styles';
import { Col, Dropdown, Menu, Row, Collapse, Space } from 'antd';
import { Content } from 'antd/lib/layout/layout';
import { DownOutlined } from '@ant-design/icons';



interface ServiceNavigationProps {
  tabs: {

    name: string,
    tab: string
  }[],
}

const CategoriesResponsive: FC<ServiceNavigationProps> = ({ tabs }) => {
  const ref = useRef<HTMLDivElement>(null);

  const { Panel } = Collapse;

  const { layoutStore } = useStores();



  useOutsideClickClose(ref, () => layoutStore.isOpenService && layoutStore.hideService());


  // const menu = (
  //   <Menu className='categories-menu-small'>

  //     {tabs.map(({ name, tab }) => (
  //       <Menu.Item key={`${tab}-tab`} className='categories-menu-item-small'>
  //         <Col span={24} className='categories-menu-item-small'>
  //           <Link to={`/${tab}`} onClick={layoutStore.hideService}>
  //             {name}
  //           </Link>
  //         </Col>
  //       </Menu.Item>
  //     ))}
  //   </Menu>
  // )



  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: (
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
              1st menu item
            </a>
          ),
        },
        {
          key: '2',
          label: (
            <a target="_blank" rel="noopener noreferrer" href="https://www.aliyun.com">
              2nd menu item
            </a>
          ),
        },
        {
          key: '3',
          label: (
            <a target="_blank" rel="noopener noreferrer" href="https://www.luohanacademy.com">
              3rd menu item
            </a>
          ),
        },
      ]}
    />
  );

  <>


  </>


  return (
    < >


      {/* <div className='wrapper'> */}

      <Dropdown overlay={menu} placement="bottomLeft"  >
        <a onClick={e => e.preventDefault()}>
          <Space>
            Hover me, Click menu item
            <DownOutlined />
          </Space>
        </a>
      </Dropdown>



      {/* </div> */}



    </ >
  );
};

export default observer(CategoriesResponsive);





