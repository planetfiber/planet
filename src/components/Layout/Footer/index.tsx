import React, { FC } from 'react';
import * as styles from './styles';

interface Props {
  size?: 'small' | 'middle' | 'large';
}

const Footer: FC<Props> = ({ size = 'middle' }) => (
  <styles.Footer >
    <span>&#169; 2021 PlanetFiber | </span>
    <span> All Right Reserved  | </span>
    <span> Terms | </span>
    <span> Privacy Policy</span>
  </styles.Footer >
);

export default Footer;
