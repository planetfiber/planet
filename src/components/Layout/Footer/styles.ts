import styled from '@styles';

export const Footer = styled.div`
  height: 48px;
  width:100%;
  background: #19293B;
  border-radius: 0px 0px 30px 30px;
  display: flex;
  justify-content: center;
  align-items: center;

  span {
    color: #fff;
    font-weight: 300;
    font-size: 18px;
    line-height: 21px
  }
  
`;
