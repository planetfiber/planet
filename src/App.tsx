import React from 'react';
import './App.css';
import Layout from '@components/Layout';
import { Redirect, Route, BrowserRouter as Router, } from 'react-router-dom';

import GlobalStyles from '@styles/GlobalStyles';
import Home from './screens/Home'
import Cables from './screens/Categories/Products';
import Product from '@screens/Product';
import AboutUs from '@screens/AboutUs';
import Partners from '@screens/Partners';


function App() {


  return (
    <div className="App">

      <Router>
        <Layout>
          <Route exact path="/">
            <Redirect to="/general" />
          </Route>
          <Route path="/general" component={Home} />
          <Route path='/about_us' component={AboutUs} />
          <Route path='/partner' component={Partners} />
          {/* <Route path='/product/' component={Cables} /> */}
          <Route path='/product/categoryIds/:id' component={Cables} />
          <Route exact path='/product/:id' component={Product} />
        </Layout>
      </Router>

      <GlobalStyles />
    </div >
  );
}

export default App;
