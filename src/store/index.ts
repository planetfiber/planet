import { createContext, useContext } from 'react';

import AuthStore from './AuthStore';
import ThemeStore from './ThemeStore';
import LayoutStore from './LayoutStore';

export type RootStoreType = RootStore;

export class RootStore {
  authStore: AuthStore;
  themeStore: ThemeStore;
  layoutStore: LayoutStore;

  constructor() {
    this.authStore = new AuthStore(this);
    this.themeStore = new ThemeStore(this);
    this.layoutStore = new LayoutStore(this);
  }
}

const StoresContext = createContext(new RootStore());

export const useStores = () => useContext(StoresContext);
export const useAuthStore = () => useStores().authStore;
