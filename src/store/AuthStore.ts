import { action, computed, makeObservable, observable } from 'mobx';

// import AuthService from '@api/services/AuthService';

import type { RootStoreType } from '.';

class AuthStore {
  root: RootStoreType;

  loading = false;
  errors = null;
  errorMessage = '';
  sessionId: string | null = null;
  authorization: string | null = null;

  constructor(root: RootStoreType) {
    this.root = root;

    makeObservable(this, {
      loading: observable,
      errors: observable,
      sessionId: observable,
      errorMessage: observable,
      authorization: observable,
      logout: action,
      // setToken: action,
      // getToken: computed,
      isAuthenticated: computed,
    });

    const token = localStorage.getItem('myToken');
    const sessionId = localStorage.getItem('sessionId');

    if (token) {
      this.authorization = `Bearer ${token}`;
      this.sessionId = sessionId;
    }
  }

  get isAuthenticated() {
    return Boolean(this.authorization) && Boolean(this.sessionId);
  }

  // setToken(token: string | null) {
  //   this.authorization = `Bearer ${token}`;
  //   localStorage.setItem('myToken', this.authorization);
  // }

  // setSessionId(sessionId: string) {
  //   this.sessionId = sessionId;
  //   localStorage.setItem('sessionId', sessionId);
  // }

  // get getToken() {
  //   if (this.authorization) {
  //     return this.authorization.match(/(?:Bearer\s+)?(\w+\.\w+\.\w+)/)?.[1];
  //   }
  //   return null;
  // }

  // login = flow(function* login(this: AuthStore, body: any) {
  //   runInAction(() => {
  //     this.loading = true;
  //   });

  //   try {
  //     const data = yield AuthService.login(body);
  //     const { token, sessionId } = data;

  //     runInAction(() => {
  //       this.setToken(token);
  //       this.setSessionId(sessionId);
  //       this.errors = null;
  //       this.errorMessage = '';
  //     });
  //   } catch (info) {
  //     runInAction(() => {
  //       this.errors = info.errors;
  //       this.errorMessage = info.errorMessage;
  //     });

  //     throw info;
  //   } finally {
  //     runInAction(() => {
  //       this.loading = false;
  //     });
  //   }
  // });

  logout() {
    this.authorization = null;
    localStorage.removeItem('myToken');
    // TODO: call after implementation this method
    // AuthService.logout();
  }
}

export default AuthStore;
