// https://github.com/facebook/create-react-app/issues/6880#issuecomment-486636121
import { useEffect } from 'react';

export const useMount = (fn: Function) => {
  useEffect(() => {
    try {
      if (typeof fn === 'function') fn();
      else throw new TypeError('Argument that passed to useMount is not a function');
    } catch (error) {
      console.error(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};
