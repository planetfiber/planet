import { useEffect } from 'react';

import { useLocation } from 'react-router-dom';

import capitalize from '@helpers/capitalize';

const useDocumentTitle = (title?: string) => {
  const { pathname } = useLocation<Location>();
  const pathTitle = pathname.split('/')[1];

  useEffect(() => {
    document.title = `Caiman Network Management ${title || capitalize(pathTitle)}`;
  }, [pathTitle, title]);
};

export default useDocumentTitle;
