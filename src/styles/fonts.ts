// import SegoeUILight_woff from '../assets/fonts/SegoeUI-Light.woff';
// import SegoeUILight_woff2 from '../assets/fonts/SegoeUI-Light.woff2';
// import SegoeUI_woff from '../assets/fonts/SegoeUI.woff';
// import SegoeUI_woff2 from '../assets/fonts/SegoeUI.woff2';
// import SegoeUIBold_woff from '../assets/fonts/SegoeUI-Bold.woff';
// import SegoeUIBold_woff2 from '../assets/fonts/SegoeUI-Bold.woff2';
// import SegoeUISemiBold_woff from '../assets/fonts/SegoeUI-SemiBold.woff';
// import SegoeUISemiBold_woff2 from '../assets/fonts/SegoeUI-SemiBold.woff2';
import AcuminLight_otf from '../assets/fonts/Acumin/Acumin-Pro-Light.otf';
import AcuminRegular_otf from '../assets/fonts/Acumin/Acumin-Pro-Medium.otf';
import AcuminBold_otf from '../assets/fonts/Acumin/Acumin-Pro-Bold.otf';

import MontserratLight_ttf from '../assets/fonts/Montserrat/Montserrat-Light.ttf';
import MontserratRegular_ttf from '../assets/fonts/Montserrat/Montserrat-Regular.ttf';
import MontserratBold_ttf from '../assets/fonts/Montserrat/Montserrat-Bold.ttf';

export const fontFaces = () => `
  @font-face {
    font-family: Montserrat;
    src:  url(${MontserratLight_ttf}) format('truetype');
    font-weight: 300;
    font-style: normal;
    font-display: swap;
  }
  @font-face {
    font-family: Montserrat;
    src:  url(${MontserratRegular_ttf}) format('truetype');
    font-weight: 400;
    font-style: normal;
    font-display: swap;
  }
  @font-face {
    font-family: Montserrat;
    src:  url(${MontserratBold_ttf}) format('truetype');
    font-weight: 700;
    font-style: normal;
    font-display: swap;
  }
  @font-face {
    font-family: Acumin;
    src: url(${AcuminLight_otf}) format('opentype');
    font-weight: 300;
    font-style: normal;
    font-display: swap;
  }
  @font-face {
    font-family: Acumin;
    src: url(${AcuminRegular_otf}) format('opentype');
    font-weight: 400;
    font-style: normal;
    font-display: swap;
  }
  @font-face {
    font-family: Acumin;
    src:  url(${AcuminBold_otf}) format('opentype');
    font-weight: 700;
    font-style: normal;
    font-display: swap;
  }
`;

export const fonts = {
  // ----- font-weights -> regular(300), medium(400), bold(700)
  primary: 'Montserrat, sans-serif',
  secondary: 'Acumin, sans-serif',
};

export const fontWeights = {
  regular: 300,
  medium: 400,
  bold: 700,
};
