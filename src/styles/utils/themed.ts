// import type { ThemeType } from '@styles/theme/themes';

type Modes = { light: string; dark: string };

export const themed = (modes: Modes) => (props: { theme: any }) => modes;
