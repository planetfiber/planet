import React, { useState } from 'react'
import MainContent from '../MainContent';
import Carousel from './Carousel';
import { Col, Row } from 'antd';
import { useMount } from '@decorators/hooks/useMount';
import SectionsService from '@api/services/SectionsService';
import FilesService from '@api/services/FilesService';
import Loader from '@screens/Loader';

const Home = () => {
  const [bestSales, setBestSales] = useState([]);
  const [specialOffers, setSpecialOffers] = useState([])
  const [slider, setSlider] = useState([])


  useMount(() => {
    SectionsService.get().then(({ data }) => {
      setBestSales(data[0].products)
      setSpecialOffers(data[1].products)
    })
  })


  useMount(() => {
    FilesService.getSlider().then(({ data }) => {
      setSlider(data)
    })
  })

  return (
    <>
      {(!slider.length && !bestSales.length && !specialOffers.length )?
        <Loader /> :

        <Row >
          <Col span={24}>
            <Carousel data={slider} />
          </Col>
          <Col span={24}><MainContent title='Best Sales' cards={bestSales} /></Col>
          <Col span={24}> <MainContent title='Special Offer' cards={specialOffers} /></Col>
        </Row>
      }
    </>
  )
}

export default Home;
