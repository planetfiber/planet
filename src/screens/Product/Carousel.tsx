import React, { FC, useState } from "react";
// import "antd/dist/antd.css";

import leftArrow from './arrow-left.svg'
import rightArrow from './arrow-right.svg'

import { Col,Image } from "antd";
import { BASE_URL } from "@api/config";
import NoImage from '@assets/images/NoImage.png'
import "./index.css";





const Carousel: FC<any> = ({ data }) => {
  const [index, setIndex] = useState(0);



  return (

    <Col md={24} lg={10} xl={8} className="gallery-slideshow" >
      <div
        className="slideshow-slider"
        style={{ transform: `translate3d(${-index * 100}%, 0, 0)` }}
      >
        {data?.length ? data.map(({ path, name, extension }: any, index: any) => (
          <div
            className="gallery-slide"
            key={`${index}-slide`}
          >
            <Image alt={name} src={`${BASE_URL}/${path}/${name}${extension}`} preview={{ mask:false}}  />
          </div>
        )) :
          <div
            className="gallery-slide"
            key={index * 10125}
          >
            <Image alt='no-slide' src={`${NoImage}`} />

          </div>
        }
      </div>

      <div className="arrow-container">
        <div className={`arrow${index === 0 ? " in-active" : ""}`} style={{ backgroundImage: `url(${leftArrow})` }}
          onClick={() => {

            if (index > 0) {
              setIndex(prev => prev - 1)
            }
          }}
        />
        <div className={`arrow${index === data?.length - 1 ? " in-active" : ""}`} style={{ backgroundImage: `url(${rightArrow})` }}

          onClick={() => {
            if (index < data?.length - 1) {
              setIndex(prev => prev + 1)
            }
          }}
        />
      </div>

      <div className="gallery-row" >
        {data?.length ? data.map(({ path, name, extension }: any, idx: number) => (
          <div
          key={`${idx}-gallery-row`}
            className={`gallery-col${index === idx ? " active" : ""}`}
            style={{ backgroundImage: `url(${BASE_URL}/${path}/${name}${extension})`, transform: `translate3d(${-index * 85}%, 0, 0)` }}
            onClick={() => {
              setIndex(idx);
            }}
          >
            <img alt={name} src={`${BASE_URL}/${path}/${name}${extension}`} />

          </div>
        ))
          :
          <div
            className={`gallery-col `}
            key={`${index}-no-slide`}
          >
            <img alt='no-slide' src={`${NoImage}`}/>
          </div>

        }
      </div>


    </Col>

  );
}


export default Carousel