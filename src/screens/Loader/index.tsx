import React from "react";
import './styles.css'

const Loader = () => {
  return <div className="loader"><p>LOADING...</p></div>
}

export default Loader