export type ID = string | number;

export interface ColorData {
  color: string;
  lines: number;
  availableLines: number[];
}
