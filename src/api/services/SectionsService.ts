import { request } from '@utils/request';
import { SECTIONS_URL } from '@api/config';
// import { tryCatchProxy } from '../../decorators/tryCatchProxy';
import { tryCatchProxy } from '@decorators/tryCatchProxy';


@tryCatchProxy
class SectionsService {
  async get() {
    return request(`${SECTIONS_URL}`, { method: 'GET' });
  }

}

export default new SectionsService();
