import { request } from '@utils/request';
import { BASE_URL, SLIDER_URL } from '@api/config';
// import { tryCatchProxy } from '../../decorators/tryCatchProxy';
import { tryCatchProxy } from '@decorators/tryCatchProxy';


@tryCatchProxy
class FilesService {
  async get(path:string, name:string, extension:string) {
    return request(`${BASE_URL}/${path}/${name}${extension}`, { method: 'GET' });
  }

  async getSlider() {
    return request(`${ SLIDER_URL}`, { method: 'GET' });
  }

}

export default new FilesService();
