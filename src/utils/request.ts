import {
  ServerError,
  ResponseError,
  UnauthorizedError,
  // UnprocessableEntityError,
} from './errorHandler';

export const inflight = {
  requests: {} as Record<string, AbortController>,
  add(name: string, controller: AbortController) {
    this.requests[name] = controller;
  },
  abort(name: string) {
    this.requests[name]?.abort();
    delete this.requests[name];
  },
};

/**
 * Parses the JSON returned by a network request
 */
function parseJSON(response: Response): Promise<any> | null {
  if (response.status === 204 || response.status === 205 || response.status === 401) {
    return null;
  }
  return response.json();
}

/**
 * Checks if a network request came back fine, and throws an error if not
 */
async function checkStatus(response: Response): Promise<Response> {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  if (response.status === 401) {
    const info = await parseJSON(response);
    localStorage.removeItem('myToken');
    throw new UnauthorizedError(response, info);
  }

  // NEEDS TO FIX
  // if (response.status === 422) {
  //   const info = await parseJSON(response);
  //   throw new UnprocessableEntityError(response, info);
  // }

  if (response.status >= 500) {
    throw new ServerError(response);
  }

  return response;
}

/**
 * Requests a URL, returning a promise
 */
export async function request(
  url: string,
  options?: RequestInit & { name?: string },
): Promise<any | { err: ResponseError }> {
  const controller = new AbortController();

  if (options?.name) {
    // --- think about aborting request before refetch
    // inflight.abort(options.name);
    inflight.add(options.name, controller);
  }

  const fetchResponse = await fetch(url, {
    ...options,
    method: options?.method ?? 'GET',
    headers: {
      ...options?.headers,
      Authorization: localStorage.getItem('myToken') ?? '',
      'x-app-session': localStorage.getItem('sessionId') ?? '77777777',
    },
    signal: controller.signal,
  });
  const response = await checkStatus(fetchResponse);
  return parseJSON(response);
}
