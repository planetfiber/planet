import React from 'react';
import Loadable from 'react-loadable';

import { LoadingPage } from '@planetkit/LoadingPage';

export const defaultLoadable = (componentToLoad: Loadable.Options<any, any>['loader']) =>
  Loadable({
    loading: () => <LoadingPage />,
    loader: componentToLoad,
    timeout: 3000,
  });

export const hiddenLoadable = (componentToLoad: Loadable.Options<any, any>['loader']) =>
  Loadable({
    loading: () => null,
    loader: componentToLoad,
  });
